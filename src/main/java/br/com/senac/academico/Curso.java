package br.com.senac.academico;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Curso extends Entidade {

    @Column(length = 45)
    private String nome;

    @JoinColumn(name = "idProfessor", nullable = false)
    @ManyToOne
    private Professor professor;

    @JoinTable(name = "CursoAluno", joinColumns = {
        @JoinColumn(name = "IdCurso", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "IdAluno", referencedColumnName = "id")})

    @ManyToMany
    private List<Aluno> alunos;

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

}
